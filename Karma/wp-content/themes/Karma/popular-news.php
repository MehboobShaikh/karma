

	<?php 
	// the query
		$popular_post = new WP_Query(array('post_status'=>'publish','post_type'=>'news','meta_key'=>'post_views_count','orderby'=>'meta_value_num','posts_per_page'=>4)); ?>
	 
	<?php if ( $popular_post->have_posts() ) : ?>
	 
	 
	    <!-- the loop -->
	    <?php while ($popular_post->have_posts()) : $popular_post->the_post(); ?>

			<div class="popular-news-row1">
				<div class="popular-news-col1">
					<div class="popular-news-image">
						<img src="<?php echo get_template_directory_uri().'/img/039.jpg'; ?>">
					</div>
					<div class="popular-news-content">
						<span class="popular-post-title"><?php echo get_field('news_title'); ?></span>
						<p><?php echo get_the_excerpt() ?></p>
						<a href="<?php echo get_permalink(); ?>"><button class="more-button">More</button></a>
					</div>
				</div>
			</div>

	    <?php endwhile; ?>
	 
	    <?php wp_reset_postdata(); ?>
	 
	<?php else : ?>
	    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
	<?php endif;
	wp_reset_postdata();

?>