	<?php 
	// the query
		$latest_post = new WP_Query(array('post_status'=>'publish','post_type'=>'news','posts_per_page'=>1)); ?>
	 
	<?php if ( $latest_post->have_posts() ) : ?>
	 
	 
	    <!-- the loop -->
	    <?php while ($latest_post->have_posts()) : $latest_post->the_post(); ?>

	    	<div class="latest-news-image">
	    		<img src="<?php echo get_template_directory_uri().'/img/039.jpg'; ?>">
				<?php //$img = get_field('news_image'); echo $img['url']; ?>
	    	</div>
			<div class="latest-news-content">
	    		<h4 class="latest-news-title"><a href="<?php the_permalink(); ?>"><?php echo get_field('news_title'); ?></a></h4>
	    		<span id="post-date"><?php the_date('l, F j, Y'); ?></span>
	    		<p class="latest-news-content">
	    			<?php echo get_the_excerpt(); ?>
	    		</p>
	    		<a href="<?php echo get_permalink(); ?>"><button class="read-more-button">read more news</button></a>
			</div>

	    <?php endwhile; ?>
	 
	    <?php wp_reset_postdata(); ?>
	 
	<?php else : ?>
	    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
	<?php endif;
	wp_reset_postdata();

?>