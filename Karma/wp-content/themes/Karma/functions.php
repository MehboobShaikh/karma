<!-- 

PHP 			=> 	Underscore( _ )
HTML / CSS 		=> 	Hyphen( - )
WORDPRESS SLUGs => 	Hyphen( - )
FILE NAME 		=> 	Hyphen( - )

 -->

<?php

	// importing files like style.css and javaScript files etc

	function firsttheme_resources(){
		wp_enqueue_style('style',get_template_directory_uri().'/style.css');

	}

	add_action('wp_enqueue_scripts','firsttheme_resources');

	// regitering new Menu

	//For Footer Recipies
	function firsttheme_register_new_menu(){
		register_nav_menu('new-menu',__('Custom Menu'));
	}

	add_action('init','firsttheme_register_new_menu');


	function firsttheme_custom_widget(){
		
		register_sidebar(array(
			'name' => __('Footer Area 1','firsttheme'),
			'id' => 'footer1',
			'description' => 'Drop your widgets to appear in footer column 1 of the page',
			'before_widget' => '<div class="widget-item">',
			'after_widget' => '</div>',
		));
		
		register_sidebar(array(
			'name' => __('Footer Area 2','firsttheme'),
			'id' => 'footer2',
			'description' => 'Drop your widgets to appear in footer column 2 of the page',
			'before_widget' => '<div class="widget-item">',
			'after_widget' => '</div>',
		));
		
		register_sidebar(array(
			'name' => __('Footer Area 3','firsttheme'),
			'id' => 'footer3',
			'description' => 'Drop your widgets to appear in footer column 3 of the page',
			'before_widget' => '<div class="widget-item">',
			'after_widget' => '</div>',
		));
		
		register_sidebar(array(
			'name' => __('Footer Area 4','firsttheme'),
			'id' => 'footer4',
			'description' => 'Drop your widgets to appear in footer column 4 of the page',
			'before_widget' => '<div class="widget-item">',
			'after_widget' => '</div>',
		));
	}

	add_action('widgets_init','firsttheme_custom_widget');


	

//===================================== POST VIEWS ========================================

	// function to display number of posts.

	function getPostViews($postID){
	    $count_key = 'post_views_count';
	    $count = get_post_meta($postID, $count_key, true);
	    if($count==''){
	        delete_post_meta($postID, $count_key);
	        add_post_meta($postID, $count_key, '0');
	        return "0 View";
	    }
	    return $count.' Views';
	}

	// function to count views.
	function setPostViews($postID) {
	    $count_key = 'post_views_count';
	    $count = get_post_meta($postID, $count_key, true);
	    if($count==''){
	        $count = 0;
	        delete_post_meta($postID, $count_key);
	        add_post_meta($postID, $count_key, '0');
	    }else{
	        $count++;
	        update_post_meta($postID, $count_key, $count);
	    }
	}




?>