<?php
	
	get_header();
	
	if(have_posts()) : 
		while(have_posts()) : the_post(); ?>


			<article class="page">

				<div class="page-post-content post-content">
					<?php echo get_field(''); ?>
				</div>

			</article>

	<?php endwhile;

	else :
		echo '<p>There is no Post Found</p>';

	endif;

	get_footer();

?>