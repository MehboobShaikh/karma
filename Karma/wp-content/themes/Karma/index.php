<?php
	
	get_header();
	
	if(have_posts()) : 
		while(have_posts()) : the_post(); ?>

	<div class="columns section1 clearfix">
		<div class="column1 welcome">
			<div class="post-content">
				<h2><?php echo get_field('title');?></h2>
				<?php echo get_field('description'); ?>
			</div>
		</div>

		<div class="column2 welcome-image">
			<img src="<?php echo get_template_directory_uri().'/img/039.jpg'?>">
		</div>

	</div>

	<div class="section2 clearfix">
		<div class="latest-news-section">

			<h2>Latest News</h2>
		
			<?php get_template_part('latest-news'); ?>
			
		</div>

		<div class="popular-news-section">

			<h2>The Most Popular News</h2>

			<?php get_template_part('popular-news'); ?>

		</div>

	</div>

	<?php endwhile;

	else :
		echo '<p>There is no Post Found</p>';

	endif;

	get_footer();

?>