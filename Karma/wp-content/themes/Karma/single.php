<?php
	
	get_header();

	
	if(have_posts()) : 
		while(have_posts()) : the_post(); 
			setPostViews(get_the_ID());
			?>
		
		<article class="post single">
			<div class="single-post-title">
				<h2><?php echo get_field('news_title'); ?></h2>
			</div>

			<div class="single-post-content">
				<?php echo get_field('news_detail'); ?>
			</div>

		</article>

	<?php endwhile;

	else :
		echo '<p>There is no Post Found</p>';

	endif;

	// get_template_part('form');

	get_footer();

?>