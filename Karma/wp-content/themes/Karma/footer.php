
<?php
if(is_front_page()){
?>
<div class="footer-row">
	<?php 
	for($i = 0; $i < 3; $i++){ ?>
	<div class="footer-col1"><h4 class="menu-category"><?php $category = get_categories();echo $category[$i]->name;?></h4>
		<div class="menu-list">
			<?php
				$menu_list = new WP_Query(array('post_type'=>'news','cat'=>$category[$i]->term_id, 'posts_per_page'=>5,'orderby'=>'title'));
				if($menu_list->have_posts()){
					while($menu_list->have_posts()){
						$menu_list->the_post(); ?>
						<div class="menu-list-item"><a href="<?php echo get_post_permalink(); ?>">
							<?php echo get_field('news_title'); ?>
						</a></div>
					<?php }
				}else{ ?>
						<div class="menu-list-item"><?php echo "No Dishes"; ?></div>
				<?php }wp_reset_postdata();
			?>
		</div>
	</div>	<?php } ?>


<!-- 	<div class="footer-col1"><h4 class="menu-category"><?php $category = get_categories();echo $category[1]->name;?></h4>
	<div class="menu-list">
		<?php
			$menu_list = new WP_Query(array('post_type'=>'news','cat'=>$category[1]->term_id, 'posts_per_page'=>5,'orderby'=>'title'));
			if($menu_list->have_posts()){
				while($menu_list->have_posts()){
					$menu_list->the_post(); ?>
					<div class="menu-list-item"><a href="<?php echo get_post_permalink(); ?>">
						<?php echo get_field('news_title'); ?>
					</a></div>
				<?php }
			}else{ ?>
					<div class="menu-list-item"><?php echo "No Dishes"; ?></div>
			<?php }wp_reset_postdata();
		?>
	</div>
</div>	


<div class="footer-col1"><h4 class="menu-category"><?php $category = get_categories();echo $category[2]->name;?></h4>
	<div class="menu-list">
		<?php
			$menu_list = new WP_Query(array('post_type'=>'news','cat'=>$category[2]->term_id, 'posts_per_page'=>5,'orderby'=>'title'));
			if($menu_list->have_posts()){
				while($menu_list->have_posts()){
					$menu_list->the_post(); ?>
					<div class="menu-list-item"><a href="<?php echo get_post_permalink(); ?>">
						<?php echo get_field('news_title'); ?>
					</a></div>
				<?php }
			}else{ ?>
					<div class="menu-list-item"><?php echo "No Dishes"; ?></div>
			<?php }wp_reset_postdata();
		?>
	</div>
</div> -->
	


	<div class="footer-col4"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt explicabo sequi, corporis eligendi, ullam rerum facere esse dolorum natus sunt asperiores, similique atque quos eveniet laudantium eos veritatis soluta distinctio!</p>
	</div>
</div>
<?php } ?>

<footer class="site-footer">
	
	<a class="clearfix copyright" style="margin-right: 10%;float: right;" href="<?php echo home_url(); ?>">&copy;Copyright 2018</a>

</footer>
</div>

<?php wp_footer(); ?>
</body>
</html>