<!DOCTYPE html>
<html>
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<meta name="viewport" content="width=device-width">
		<title><?php bloginfo('name'); ?></title>
		<?php wp_head(); ?>
	</head>

<body <?php body_class(); ?>>

	<div class="container">

	<!-- Site Header -->
	<header class="site-header">
		<h1><a class="site-name" href="<?php echo home_url(); ?>"> <?php bloginfo('name'); ?></a></h1>
	</header>	<!-- Site Header -->

	<!-- Slider Goes Here -->
	<?php // get_template_part('slider'); ?>

	<nav class="site-nav">
		<?php
			$args = array(
				'menu' => 'main-menu'
			);
		?>
		<?php wp_nav_menu($args); ?>
	</nav>
	

	